import csv

#stop_words = ['lot', 'lov', 'help', 'list', 'fun', 'awesom', 'tim', 'alot', 'see', 'get', 'someth']

stop_words = set()
csvfileread = open('frequentitemsets0.txt', 'rt', encoding = 'utf-8')
reader = csv.reader(csvfileread, delimiter=',')

frequent_items = []
k=-1
for row in reader:
    if len(row)==1:
        k+=1
        frequent_items.append([row[0]])
        print(frequent_items[k-1])
    else:
        items = [item.strip() for item in row]
        del items[-1]
        items.sort()
        frequent_items[k].append(items)

first_words = [item[0] for item in frequent_items[0] if type(item) is list and len(item)==1]
stop_words = set(first_words)
for row in frequent_items:
    cur_words = set([item[0] for item in row if type(item) is list and len(item)==1])
    stop_words = stop_words & cur_words

clean_items = []
for i in range(len(frequent_items)):
    clean_items.append([frequent_items[i][0]])
    for j in range(1,len(frequent_items[i])):
        cell = frequent_items[i][j]
        if not(type(cell) is list and len(cell)==1 and cell[0] in stop_words):
            clean_items[i].append(cell)

print(clean_items)
frequent_items = clean_items
result = []
for i in range(len(frequent_items)):
    result.append([frequent_items[i][0]])
    #print(result)
    for j in range(i+1, len(frequent_items)):
        result[len(result)-1].append([frequent_items[j][0]])
        for x in frequent_items[i]:
            for y in frequent_items[j]:
                if type(x) is list and not x==[]:
                    if type(y) is list and not y==[]:
                        res = list(set(x).symmetric_difference(set(y)))
                        if res==[]:
                            result[len(result)-1].append(x)
    print((i+1)/(len(frequent_items))*100)
        #result[len(result)-1].append(set(frequent_items[i])&set(frequent_items[j]))

w = (open("matches_2_WORDS_NO_STEM_SUPPORT_10.txt", "w", encoding='utf-8'))
for row in result:
    w.write("COMMON WORDS OF ")
    w.write(row[0])
    w.write(" WITH: ")
    for cell in row:
        if type(cell) is list:
            if cell[0].isdigit():
                w.write("\n")
                w.write("-> ")
                w.write(cell[0])
                w.write("\n")
            else:
                if len(cell)>1:
                    w.write(" ".join(cell))
                    w.write(", ")
    w.write('\n')
    w.write('\n')
w.close()

w = (open("table_test.csv", "w", encoding='utf-8'))
w.write('#,')
row=result[0]
w.write(row[0])
w.write(",")
for cell in row:
   if type(cell) is list:
       if cell[0].isdigit():
           w.write(cell[0])
           w.write(",")
w.write('\n')

skip=0
for row in result:
    w.write(row[0])
    w.write(",")
    for i in range(skip):
        w.write('#,')
    skip+=1
    for cell in row:
        if type(cell) is list:
            if cell[0].isdigit():
                if len(cell)==1:
                    w.write("#")
                w.write(',')
            else:
                if len(cell)>1:
                    w.write(" ".join(cell))
                    w.write("; ")
    w.write('\n')
w.close()