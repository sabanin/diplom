import os
import nltk, time, psycopg2, subprocess, shlex, requests, json, operator

from pymongo import MongoClient
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.collocations import *
from nltk.corpus import wordnet

try:
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='12345678'")
except:
    print("I am unable to connect to the database")

features_cursor = conn.cursor()
features_cursor.execute("SELECT * FROM synonyms")
features = features_cursor.fetchall()
features_dict = {}

for feature in features:
    all = eval(feature[0])
    main = eval(feature[1])
    res = 0
    for (x,y) in all:
        res+=y
    main = (main[0], res)
    for (x,y) in all:
        features_dict[x] = main

print(features_dict)
print(len(features_dict))

reviews_cursor = conn.cursor()
reviews_cursor.execute("SELECT * FROM colocations3");
rows = reviews_cursor.fetchall()

i=1;
for row in rows:
    all = eval(row[3])
    for (x,y) in all:
        feature = x+"_"+y
        if (features_dict.get(feature) != None):
            main = features_dict[feature]
            if(main[1]>5):
                r = requests.post('http://review-sentiment.herokuapp.com/reviews/rest', data = {'appid':row[1], 'comment':row[2], 'feature':feature, 'mainfeature':main[0], 'frequency':main[1], 'negative':row[4], 'positive':row[5]})
    print(i/106148*100)
    i+=1