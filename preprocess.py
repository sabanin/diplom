import os
import time, csv
import nltk, psycopg2
stopwords = {}
with open('stopwords.txt', 'rU') as f:
    for line in f:
        stopwords[line.strip()] = 1

done = 0
start = time.time()

csvfileread = open('dataset.csv', 'rt', encoding = 'utf-8')
spamreader = csv.reader(csvfileread, delimiter='|')
reviews = []
i = 0
text_file = open("Output.txt", "w", encoding='utf-8')
csvfilewrite = open('output.csv', 'w', encoding='utf-8')
#csvfilewrite_nouns = open('output_nouns.csv', 'w', encoding='utf-8')
spamwriter = csv.writer(csvfilewrite, delimiter='|')
#spamwriter_nouns = csv.writer(csvfilewrite, delimiter='|')

try:
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='12345678'")
except:
    print("I am unable to connect to the database")

cur = conn.cursor()
for row in spamreader:
    try:
        review = row[24]
        sentences = nltk.sent_tokenize(review.lower())
        for sentence in sentences:
            words = []
            tokens = nltk.word_tokenize(sentence)
            # text = [word for word in tokens if word not in stopwords]
            tagged_text = nltk.pos_tag(tokens)
            for word, tag in tagged_text:
                if word not in stopwords:
                    words.append({"word": word, "pos": tag})
            cur.execute("INSERT INTO rev3 (reviewid, business, text, words) VALUES (%s, %s, %s, %s)",(i, row[0], sentence, str(words)))
            conn.commit()

        print(i/106148*100)
        i+=1
    except IndexError:
        print("Index error")
        i+=1
    except UnicodeEncodeError:
        print("Encode error")
        i+=1

#sorted_x = sorted(stop_words.items(), key=operator.itemgetter(1))
#sorted_x.reverse()
#text_file.write(",".join("(%s,%s)" % tup for tup in sorted_x))
#print(sorted_x)
text_file.close()
csvfilewrite.close()
