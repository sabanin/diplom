import os
import time, psycopg2

from pymongo import MongoClient
from nltk.stem.wordnet import WordNetLemmatizer


try:
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='12345678'")
except:
    print("I am unable to connect to the database")


lem = WordNetLemmatizer()
reviews_cursor = conn.cursor()
reviews_cursor.execute("SELECT * FROM rev1");
rows = reviews_cursor.fetchall()

corpus_cursor = conn.cursor()
i=1;
for review in rows:
    nouns = []
    words = [word for word in eval(review[3]) if word["pos"] in ["NN", "NNS"]]
    for word in words:
        nouns.append(lem.lemmatize(word["word"]))

    corpus_cursor.execute("INSERT INTO corp1 (reviewid, business, text, words) VALUES (%s, %s, %s, %s)",(review[0], review[1], review[2], str(nouns)))
    conn.commit()
    print(nouns)
    print(i/106148*100)
    i+=1
    # corpus_collection.insert({
    #     "reviewId": review["reviewId"],
    #     "business": review["business"],
    #     "text": review["text"],
    #     "words": nouns
    # })