import subprocess, shlex
#Alec Larsen - University of the Witwatersrand, South Africa, 2012 import shlex, subprocess

def RateSentiment(sentiString):
    #open a subprocess using shlex to get the command line string into the correct args list format
    p = subprocess.Popen(shlex.split("java -jar SentiStrengthCom.jar stdin sentidata C:/SentStrength_Data/"),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #communicate via stdin the string to be rated. Note that all spaces are replaced with +
    stdout_text, stderr_text = p.communicate(bytes(sentiString.replace(" ","+"), 'UTF-8'))
    #remove the tab spacing between the positive and negative ratings. e.g. 1    -5 -> 1-5
    print(stderr_text)
    print(stdout_text)
    stdout_text = stdout_text.decode('UTF-8').rstrip().replace("\t","\n")
    return stdout_text

s = RateSentiment("do not love app")
print(s)
