import os
import nltk, time, psycopg2, subprocess, shlex, requests

from pymongo import MongoClient
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.collocations import *
def RateSentiment(sentiString):
    #open a subprocess using shlex to get the command line string into the correct args list format
    p = subprocess.Popen(shlex.split("java -jar SentiStrengthCom.jar stdin sentidata C:/SentStrength_Data/"),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #communicate via stdin the string to be rated. Note that all spaces are replaced with +
    stdout_text, stderr_text = p.communicate(bytes(sentiString.replace(" ","+"), 'UTF-8'))
    #remove the tab spacing between the positive and negative ratings. e.g. 1    -5 -> 1-5
    stdout_text = stdout_text.decode('UTF-8').rstrip().replace("\t","\n")
    return stdout_text

try:
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='12345678'")
except:
    print("I am unable to connect to the database")


lem = WordNetLemmatizer()
reviews_cursor = conn.cursor()
reviews_cursor.execute("SELECT * FROM rev3");
rows = reviews_cursor.fetchall()

big_result = []
corpus_cursor = conn.cursor()
i=1;
for review in rows:
    nouns = []
    words = [word for word in eval(review[3]) if word["pos"] in ["NN", "NNS", "NNPS", "NNP", "VB", "VBD", "VBG", "VBN", "VBP", "VBZ"]]
    for word in words:
        nouns.append(lem.lemmatize(word["word"]))
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    finder = BigramCollocationFinder.from_words(nouns)
    scored = finder.score_ngrams(bigram_measures.raw_freq)
    res = sorted(bigram for bigram, score in scored)
    print (review[3])
    print (res)
    if (res!=[]):
        sentiment = RateSentiment(review[2]).split('\n')
        corpus_cursor.execute("INSERT INTO colocations3 (reviewid, business, text, colocations, negative, positive) VALUES (%s, %s, %s, %s, %s, %s)",(review[0], review[1], review[2], str(res), sentiment[1], sentiment[0]))
        conn.commit()
        # print(sentiment)
        # r = requests.post('http://localhost:8081/reviews/rest', data = {'appid':appid, 'comment':sentence, 'feature':feature, 'negative':0, 'positive':0})

    print(i/106148*100)
    i+=1

# print("DONE... Wait for counter")
# from collections import Counter
# cnt = Counter(big_result)
# result_features = set( [k for k, v in cnt.items() if v > 65] )
# # print (len (set( [k for k, v in cnt.items() if v > 25] )))
# # print (len (set( [k for k, v in cnt.items() if v > 30] )))
# # print (len (set( [k for k, v in cnt.items() if v > 35] )))
# # print (len (set( [k for k, v in cnt.items() if v > 40] )))
# # print (len (set( [k for k, v in cnt.items() if v > 45] )))
# # print (len (set( [k for k, v in cnt.items() if v > 50] )))
# # print (len (set( [k for k, v in cnt.items() if v > 55] )))
# # print (len (set( [k for k, v in cnt.items() if v > 60] )))
# # print (len (set( [k for k, v in cnt.items() if v > 65] )))
#
# for feature in result_features:
#     print(feature)
#
# reviews_cursor.execute("SELECT * FROM rev1");
# rows = reviews_cursor.fetchall()
# big_result = []
# i=1
# for review in rows:
#     nouns = []
#     words = [word for word in eval(review[3]) if word["pos"] in ["NN", "NNS"]]
#     for word in words:
#         nouns.append(lem.lemmatize(word["word"]))
#     bigram_measures = nltk.collocations.BigramAssocMeasures()
#     finder = BigramCollocationFinder.from_words(nouns, window_size=3)
#     scored = finder.score_ngrams(bigram_measures.raw_freq)
#     res = sorted(bigram for bigram, score in scored)
#     print (review[3])
#     print (res)
#     for (x,y) in res:
#         appid = review[1]
#         sentence = review[2]
#         feature = x+" "+y
#         if feature in result_features:
#             print("OUTPUT:")
#             print(appid)
#             print(sentence)
#             print(feature)
#             sentiment = RateSentiment(sentence).split('\n')
#             print(sentiment)
#             r = requests.post('http://localhost:8081/reviews/rest', data = {'appid':appid, 'comment':sentence, 'feature':feature, 'negative':sentiment[1], 'positive':sentiment[0]})
#     print(i/106148*100)
#     i+=1
