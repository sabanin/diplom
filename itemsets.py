import nltk, csv, pymining, operator
from pymining import itemmining
from nltk import word_tokenize

def leave_nouns(big_app_review_nt):
    big_app_review = word_tokenize(big_app_review_nt)
    pos_words = nltk.pos_tag(big_app_review)
    print(pos_words)
    result = []
    for w,t in pos_words:
        if 'NN' in t:
            result+=[w]
        if '.' in t:
            result+=[w]
    print(result)
    #return [w for w in result if w not in stop_words]
    stemmer = nltk.PorterStemmer()
    #stemmer = nltk.LancasterStemmer()
    return [stemmer.stem(t) for t in result]
    #return result;

csvfileread = open('output.csv', 'rt', encoding = 'utf-8')
spamreader = csv.reader(csvfileread, delimiter='|')
reviews = {}
i = 0
for row in spamreader:
    try:
        id = row[0]
        review = row[1]
        review = ' '.join(leave_nouns(review))
        if id in reviews:
            reviews[id]=reviews[id]+review
        else:
            reviews[id]=review
        print(i/212296*100)
        i+=1
    except IndexError:
        i+=1

w = (open("FEATURES_2WORDS_PORTER_STEM.txt", "w", encoding='utf-8'))
lenght = len(reviews.keys())
i = 0
for key, val in reviews.items():
    transactions= [filter(None, t.split(" ")) for t in val.split(".")]
    relim_input = itemmining.get_relim_input(transactions)
    report = itemmining.relim(relim_input, min_support=5)
    sorted_report = sorted(report.items(), key=operator.itemgetter(1))
    sorted_report.reverse()
    w.write(key+"\n")
#    w.write(",".join("(%s,%s)" % tup for tup in sorted_report)
    #w.writerow([key, report])
    output = [list(fset)+[str(support)] for (fset, support) in sorted_report if len(fset)>1]
    w.write("\n".join([", ".join(elem) for elem in output]))
    w.write("\n")
    i+=1
    print(i/lenght*100)

w.close()
