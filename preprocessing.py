import csv, nltk, re, operator
from nltk import word_tokenize
from nltk.corpus import stopwords
stop_words = ['i','it','the','and','is','this','a','my','for','of','you','have','me','that','use','on','in','so','very',
              'with','can','your','all','are','be','really','when','would','do','just','was','has','if','as','what','or','at','how','an',
              'been','only', 'does', 'now', 'every', 'there', 'about', 'much', 'also', 'had', 'from', 'will', 'even', 'they', 'than',
              'could', 'am', 'some', 'many', 'too', 'because', 'after', 'its', 'did', 'always', 'we', 'still', 'than', 'any', 'while',
              'should', 'u', 'most', 'app']#, 'everything', 'way']

def review_cleaning(tokens):
    print(tokens)
    words = []
    for w in tokens:
        word = [w.lower()]
        if word[0] == "n't":
            word[0] = "not"
        if word[0] == "ca":
            word[0] = "can"
        if word[0] == "cant":
            word[0] = "can"
            word+= ["not"]
        if word[0] == "im":
            word[0] = "i"
            word+= ["am"]
        if word[0] == "dont":
            word[0] = "do"
            word+= ["not"]
        if word[0] == "didnt":
            word[0] = "did"
            word+= ["not"]
        if word[0] == "havent":
            word[0] = "have"
            word+= ["not"]
        if word[0] == "hasnt":
            word[0] = "has"
            word+= ["not"]
        if word[0] == "wouldnt":
            word[0] = "would"
            word+= ["not"]
        if word[0] == "wo":
            word[0] = "will"
        if word[0] == "&":
            word[0] = "and"
        if word[0] == "'s":
            word[0] = "is"
        if word[0] == "'re":
            word[0] = "are"
        if word[0] == "'d":
            word[0] = "would"
        if word[0] == "'m":
            word[0] = "am"
        if word[0] == "'ve":
            word[0] = "have"
        if word[0] == "alot":
            word[0] = "lot"
        if word[0] == "ive":
            word[0] = "i"
            word += ["have"]
        if word[0] == "but" or word[0] == "however":
            word[0] = "."
        if word[0] == "!" or ("-" in word[0]) or ("*" in word[0]):
            word[0] = "."
        if word[0] == '...':
            word[0] = '.'
        if "/" in word[0]:
            word = word[0].split("/")
        if "\\" in word[0]:
            word = word[0].split("\\")
        if "$" in word[0]:
            word[0] = re.sub("[^0-9a-zA-Z]", "", word[0])
            word += ["dollars"]
        if ("." in word[0]) and (len(word[0])>1):
            word = [" . "+e for e in word[0].split(".") if e!=""]
        if word!=[] and word[0] != ".":
            word[0] = re.sub("[^0-9a-zA-Z]", "", word[0])
#        print(word)
        if word!=[] and word[0]!='':
            words+=word
    print(words)
#    for word in words:
#        if word in stop_words:
#            stop_words[word]+=1
#        else:
#            stop_words[word]=1
    return words

def leave_nouns(big_app_review_nt):
    big_app_review = word_tokenize(big_app_review_nt)
    pos_words = nltk.pos_tag(big_app_review)
    print(pos_words)
    result = []
    for w,t in pos_words:
        if 'NN' in t:
            result+=[w]
        if '.' in t:
            result+=[w]
    print(result)
    #return [w for w in result if w not in stop_words]
    #return result
    stemmer = nltk.PorterStemmer()
    #stemmer = nltk.LancasterStemmer()
    return [stemmer.stem(t) for t in result]

def clean_dots(words):
    flag = 0
    result = []
    for word in words:
        if word=='.' and flag==0:
            flag=1
            result+=[word]
        if word!='.':
            flag=0
            result+=[word]
    return result

csvfileread = open('dataset.csv', 'rt', encoding = 'utf-8')
spamreader = csv.reader(csvfileread, delimiter='|')
reviews = []
i = 0
text_file = open("Output.txt", "w", encoding='utf-8')
csvfilewrite = open('output.csv', 'w', encoding='utf-8')
#csvfilewrite_nouns = open('output_nouns.csv', 'w', encoding='utf-8')
spamwriter = csv.writer(csvfilewrite, delimiter='|')
#spamwriter_nouns = csv.writer(csvfilewrite, delimiter='|')

for row in spamreader:
    try:
        tokens = word_tokenize(row[24])
        cleaned_tokens = review_cleaning(tokens)
        words = [w for w in cleaned_tokens if w not in stop_words]
        #words = review_cleaning(tokens)
        reviews.append([row[0], words])
#        text_file.write(', '.join(words))
        review_text = ' '.join(clean_dots(words))
        #review_text_nouns = ' '.join(clean_dots(leave_nouns(words)))
        #review_text_nouns = ' '.join(clean_dots(leave_nouns(words)))
        print(review_text)
        spamwriter.writerow([row[0]]+[review_text])
        #spamwriter_nouns.writerow([row[0]]+[review_text_nouns])
        print(i/106148*100)
        i+=1
    except IndexError:
        print("Index error")
        i+=1
    except UnicodeEncodeError:
        print("Encode error")
        i+=1

#sorted_x = sorted(stop_words.items(), key=operator.itemgetter(1))
#sorted_x.reverse()
#text_file.write(",".join("(%s,%s)" % tup for tup in sorted_x))
#print(sorted_x)
text_file.close()
csvfilewrite.close()