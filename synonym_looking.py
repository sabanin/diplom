import os
import nltk, time, psycopg2, subprocess, shlex, requests, json, operator

from pymongo import MongoClient
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.collocations import *
from nltk.corpus import wordnet

def check_synonym(synsets, word):
    for synset in synsets:
        if word in synset.lemma_names():
            return True
    return False

def check_feature_synonym(synset1, synset2, feature, featureJ):
  featureX = featureJ.split("_")
  featureY = feature.split("_")
  if (featureX[0]==featureY[1] and featureX[1]==featureY[0]):
    return True
  if (featureX[0]==featureY[1] and check_synonym(synset2, featureY[0])):
    return True
  if (featureX[1]==featureY[0] and check_synonym(synset1, featureY[1])):
    return True
  return (check_synonym(synset1, featureX[0]) and check_synonym(synset2, featureX[1])) or (check_synonym(synset1, featureX[1]) and check_synonym(synset2, featureX[0]))

try:
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='12345678'")
except:
    print("I am unable to connect to the database")

reviews_cursor = conn.cursor()
reviews_cursor.execute("SELECT * FROM colocations3");
rows = reviews_cursor.fetchall()


stoplist = ["app", "love", "fun", "apps", "stop", "%"]
features_count = {}
i=1;
for review in rows:
  features = eval(review[3])
  for (word1, word2) in features:
    if word1 not in stoplist and word2 not in stoplist:
      synset1 =  wordnet.synsets(word1)
      if not check_synonym(synset1, word2) and not word1==word2 and "'" not in word1 and "'" not in word2:
        if (word1+"_"+word2) in features_count.keys():
          features_count[word1+"_"+word2]+=1
        else:
          features_count[word1+"_"+word2]=1
  i+=1
  print(i/106148*100)

corpus_cursor = conn.cursor()
ignore_list = set()
size = len(features_count)
i=1
for feature, frequency in features_count.items():
  if (frequency>=10) and (feature not in ignore_list):
    synonym_group = {}
    synonym_group[feature] = frequency
    ignore_list.add(feature)
    featureX = feature.split("_")
    synset1 = wordnet.synsets(featureX[0])
    synset2 = wordnet.synsets(featureX[1])
    for featureJ, frequencyJ in features_count.items():
      if (frequencyJ >= 10) and (featureJ not in ignore_list):
        if check_feature_synonym(synset1, synset2, feature, featureJ):
          print("SYNONIMS! "+feature+" "+featureJ)
          ignore_list.add(featureJ)
          synonym_group[featureJ] = frequencyJ
    print(synonym_group)
    sorted_x = sorted(synonym_group.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    print(str(sorted_x))
    corpus_cursor.execute("INSERT INTO synonyms (features, main) VALUES (%s, %s)",(str(sorted_x), str(sorted_x[0])))
    conn.commit()
    print(i/size*100)
  i+=1