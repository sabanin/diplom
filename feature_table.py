import operator,csv, subprocess, shlex, json, psycopg2, re, requests

def RateSentiment(sentiString):
    #open a subprocess using shlex to get the command line string into the correct args list format
    p = subprocess.Popen(shlex.split("java -jar SentiStrengthCom.jar stdin sentidata C:/SentStrength_Data/"),stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #communicate via stdin the string to be rated. Note that all spaces are replaced with +
    stdout_text, stderr_text = p.communicate(bytes(sentiString.replace(" ","+"), 'UTF-8'))
    #remove the tab spacing between the positive and negative ratings. e.g. 1    -5 -> 1-5
    stdout_text = stdout_text.decode('UTF-8').rstrip().replace("\t","\n")
    return stdout_text

def countSentiment(feature, appid, reviews, cur):
    # sentences = reviews.split(".")
    sentences = re.split("[\.!?]", reviews.lower())
    print(sentences)
    words = feature.split(" ")
    print("Sentences to process:")
    print(len(sentences))
    support = 0
    result = {-5:0, -4:0, -3:0, -2:0, -1:0, 1:0, 2:0, 3:0, 4:0, 5:0}
    for sentence in sentences:
        if set(words)<set(sentence.split(" ")):
            support+=1
            print("Sentence found:")
            print(sentence)
            sentiment = RateSentiment(sentence).split('\n')
            print(str(sentiment[0])+" "+str(sentiment[1]))
            result[int(sentiment[0])]+=1
            result[int(sentiment[1])]+=1
            r = requests.post('http://localhost:8081/reviews', data = {'appid':appid, 'comment':sentence, 'feature':feature, 'negative':sentiment[1], 'positive':sentiment[0]})
            # cur.execute("INSERT INTO reviews (appid, comment, feature, negative, positive) VALUES (%s, %s, %s, %s, %s)",(feature, appid, sentence, sentiment[0], sentiment[1]))
            # conn.commit()

    #result = sorted(result.items(), key=operator.itemgetter(0))
    pos = 0 #int((result[2]+result[3]+result[4]+result[5])/(result[1]+result[2]+result[3]+result[4]+result[5])*100)
    neg = 0 #int((result[-2]+result[-3]+result[-4]+result[-5])/(result[-1]+result[-2]+result[-3]+result[-4]+result[-5])*100)
    return "Pos: "+str(pos)+"% Neg: "+str(neg)+"% Over "+str(len(sentences))+ "sent ("+str(support)+" with feature)"

try:
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='localhost' password='12345678'")
except:
    print("I am unable to connect to the database")

cur = conn.cursor()

stop_words = set()
csvfileread = open('frequentitem_lancasterstem.txt', 'rt', encoding = 'utf-8')
reader = csv.reader(csvfileread, delimiter=',')

frequent_items = []
applications_ids = []
k=-1
for row in reader:
    if len(row)==1:
        k+=1
        frequent_items.append([row[0]])
        applications_ids.append(row[0])
        print(frequent_items[k-1])
    if len(row)>2:
        items = [item.strip() for item in row]
        items.sort()
        frequent_items[k].append(items)

print(frequent_items)
applications_ids = sorted(applications_ids)
print(applications_ids)

items_dict = {}
all_items = {}
for row in frequent_items:
    current=row[0]
    for item in row:
        if type(item) is list:
            res = item
            freq = int(res[0])
            del res[0]
            feature = ' '.join(res)
            items_dict[current].add(feature)
            if feature in all_items:
                all_items[feature]+=freq
            else:
                all_items[feature]=freq
        else:
            items_dict[current]=set();

all_items = sorted(all_items.items(), key=operator.itemgetter(1), reverse=True)
#items_dict = sorted(items_dict.items(), key=operator.itemgetter(0))
print(items_dict)
print(all_items)

w = open("feature_table.csv", "w", encoding='utf-8')
w.write('#,')
w.write(', '.join(applications_ids))
w.write('\n')
for key, value in all_items:
    if value>40:
        w.write(key)
        w.write(",")
        for id in applications_ids:
            if key in items_dict[id]:
                w.write("+")
                w.write(",")
            else:
                w.write(",")
        w.write("\n")

w.close()

r = open('output.csv', 'rt', encoding = 'utf-8')
reader = csv.reader(r, delimiter='|')
i = 0
reviews = {}
print("Preprocessing...")
for row in reader:
    try:
        id = row[0]
        review = row[1]
        if id in reviews:
            reviews[id]=reviews[id]+review
        else:
            reviews[id]=review
        i+=1
    except IndexError:
        i+=1
r.close()

w = open("SOMETHINGSOMETHING.csv", "w", encoding='utf-8')
w.write('#,')
w.write(', '.join(applications_ids))
w.write('\n')
for key, value in all_items:
    if value>35:
        w.write(key)
        w.write(",")
        for id in applications_ids:
            if key in items_dict[id]:
                print("Count sentiments for feature - "+key+" - and app "+id)
                res = countSentiment(key, id, reviews[id], cur)
                print(res)
                w.write(res)
                w.write(",")
            else:
                w.write(",")
        w.write("\n")

cur.close()
conn.close()
w.close()

